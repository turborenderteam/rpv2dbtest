
#include <Windows.h>
#include <vector>
#include <string>
#include <iostream>
#include <exception>
#include <sqlite3.h>


sqlite3 * db = nullptr;
const char* db_path = "//10.5.0.1/c/Program Files (x86)/RenderPal V2/Server/Data/RenderPal.Svr.v2db";

const std::string sql_netjobs("SELECT NetJobID, Name, _Status, Paused, Priority, Urgent, \
					   strftime('%Y/%m/%d %H:%M:%S', julianday(rpv2_netjobs.FinishDate, 'localtime')) AS \
					   FinishDate FROM rpv2_netjobs");

const std::string sql_chunks("SELECT ch.ChunkID, nj.NetJobID, ch._Frames, ch.StartTime, ch.EndTime, ch.Status, \
							 CASE \
							 WHEN ch.RenderingClient <> '' THEN ch.RenderingClient \
							 WHEN ch._RenderingClientUuid = '00000000-0000-0000-0000-000000000000' THEN '' \
							 ELSE (SELECT cl1.Alias FROM rpv2_netjobchunks AS ch1, rpv2_netjobs AS nj1, rpv2_clients AS cl1 \
							 WHERE cl1.Uuid = ch1._RenderingClientUuid) END AS RenderingClient \
							 FROM rpv2_netjobchunks AS ch, rpv2_netjobs AS nj WHERE \
							 ch.NetJobID = nj.NetJobID");

const std::string sql_clients("SELECT cl.Alias FROM rpv2_clients AS cl, rpv2_pools AS pl WHERE pl.Name = 'Default pool' \
							  AND pl.ClientList  LIKE '%'||cl.Uuid||'%'");


// ����� ������� ��� ��������� ��������
static int Sqlite3CallBack(void *data, int argc, char **argv, char **azColName)
{
	try{
		if(data == nullptr) 
			throw std::exception("data pointer is null");

		std::vector<std::string>* qset = (std::vector<std::string>*)data;

		if(qset == nullptr) 
			throw std::exception("qset pointer is null");

		for (int i=0; i < argc; i++) {
			qset->push_back(argv[i]);
		}
	}
	catch(std::exception& e) {
		return 1;
	}
	return 0;
}


void Reconnect(){
	if(db){
		sqlite3_close(db);
		db = nullptr;
	}
	if(sqlite3_open(db_path, &db) != SQLITE_OK){
		std::string err_string(sqlite3_errmsg(db));
		sqlite3_close(db);
		throw std::exception(err_string.c_str());
	}
}

std::vector<std::string> ExecuteQuery(std::string const& query) {
	std::vector<std::string> ret;

	try {
		char *zErrMsg = nullptr;
		int result = 0;
		do {
			result = sqlite3_exec(db, query.c_str(), Sqlite3CallBack, (void*)&ret, &zErrMsg);

			if(result != SQLITE_OK) {
				std::string err_msg(zErrMsg);
				sqlite3_free(zErrMsg);
				ret.clear();

				switch (result)
				{
				case SQLITE_BUSY:
					Sleep(10);
					break;
				case SQLITE_ERROR:
					throw std::exception(err_msg.c_str());
				case SQLITE_LOCKED:
				default:
					Reconnect();
				}
			}

		}while(result != SQLITE_OK);

	}catch(std::exception& e) {
		throw e;
	}
	return ret;
}



int main(int argc, const char argv[]) {

	try{
		int ret = sqlite3_open(db_path, &db);
		if (ret != SQLITE_OK) {
			const char* msg = sqlite3_errmsg(db);
			throw std::exception(("Can't open database: " + std::string(db_path)).c_str());
		}

		for(int i=0; i < 1000; i++) {

			std::vector<std::string> result = ExecuteQuery(sql_netjobs);

			result = ExecuteQuery(sql_chunks);

			result = ExecuteQuery(sql_clients);

			std::cout << "OK" << std::endl;
			Sleep(500);
		}

	}catch(std::exception& e){
		std::cout << "Fatal error: " << e.what() << std::endl;
		if(db)
			sqlite3_close(db);
		return 1;
	}
	if(db)
		sqlite3_close(db);

	return 0;
}